<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Route::view('/{any}', 'app')->where('any', '.*');



// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/category', 'CategoryController@index')->name('category.index');
// Route::post('/category', 'CategoryController@store')->name('category.store');
// Route::get('/category/create', 'CategoryController@create')->name('category.create');
// Route::get('/category/{category}', 'CategoryController@show')->name('category.show');
// Route::get('/category/{category}/edit', 'CategoryController@edit')->name('category.edit');
// Route::post('/category/{category}/update', 'CategoryController@update')->name('category.update');
// Route::get('/category/{category}/delete', 'CategoryController@destroy')->name('category.destroy');

// Route::get('/transaction', 'TransactionController@index')->name('transaction.index');
// Route::post('/transaction', 'TransactionController@store')->name('transaction.store');
// Route::get('/transaction/create', 'TransactionController@create')->name('transaction.create');
// Route::get('/transaction/{transaction}', 'TransactionController@show')->name('transaction.show');
// Route::get('/transaction/{transaction}/edit', 'TransactionController@edit')->name('transaction.edit');
// Route::post('/transaction/{transaction}/update', 'TransactionController@update')->name('transaction.update');
// Route::get('/transaction/{transaction}/delete', 'TransactionController@destroy')->name('transaction.destroy');


// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
