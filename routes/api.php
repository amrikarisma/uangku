<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/category', 'CategoryController@index');
Route::post('/category/store', 'CategoryController@store');
Route::get('/category/{id?}', 'CategoryController@show');
Route::post('/category/update/{id?}', 'CategoryController@update');
Route::delete('/category/{id?}', 'CategoryController@destroy');

Route::get('/transaction', 'TransactionController@index');
Route::post('/transaction/store', 'TransactionController@store');
Route::get('/transaction/{id?}', 'TransactionController@show');
Route::post('/transaction/update/{id?}', 'TransactionController@update');
Route::delete('/transaction/{id?}', 'TransactionController@destroy');


// Route::get('/category/{category}', 'API\CategoryController@get_type')->name('api.category');
