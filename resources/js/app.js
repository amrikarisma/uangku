require('./bootstrap');
window.Vue = require('vue');

import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

const moment = require('moment')
require('moment/locale/id')
 
Vue.use(require('vue-moment'), {
    moment
})

import routes from './router'

import VueCurrencyFilter from 'vue-currency-filter'
Vue.use(VueCurrencyFilter,
  {
    symbol : 'Rp.',
    thousandsSeparator: '.',
    fractionCount: 0,
    fractionSeparator: ',',
    symbolPosition: 'front',
    symbolSpacing: true
  })
  
Vue.component('navigation', require('./components/Nav').default);

const app = new Vue({
    el: '#app',
    router: new VueRouter(routes),

})
