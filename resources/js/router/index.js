import Home from '../components/Home.vue'

import IndexCategory from '../components/category/Category.vue'
import AddCategory from '../components/category/AddCategory.vue'
import EditCategory from '../components/category/EditCategory.vue'

import Transaction from '../components/transaction/Transaction.vue'
import AddTransaction from '../components/transaction/AddTransaction.vue'
import EditTransaction from '../components/transaction/EditTransaction.vue'

export default {
    mode: 'history',

    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: { title: 'Uangku' },
        },
        {
            path: '/category',
            name: 'category',
            component: IndexCategory,
            meta: { title: 'Tambah Kategori' }
        },
        {
            path: '/category/add',
            name: 'category.add',
            component: AddCategory,
            meta: { title: 'Tambah Kategori' }
        },
        {
            path: '/category/edit/:id',
            name: 'category.edit',
            component: EditCategory,
            meta: { title: 'Edit Kategori' }
        },
        {
            path: '/transaction',
            name: 'transaction',
            component: Transaction,
            meta: { title: 'Daftar Transaksi' }
        },
        {
            path: '/transaction/add',
            name: 'transaction.add',
            component: AddTransaction,
            meta: { title: 'Tambah Transaksi' }

        },
        {
            path: '/transaction/edit',
            name: 'transaction.edit',
            component: EditTransaction,
        }
    ]
    
}