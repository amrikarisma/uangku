@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col">
                            <div>Pemasukan <span class="text-success">{{$sum['income'] ?? ''}}</span> (All Time)</div>
                            <div>Pengeluaran <span class="text-danger">{{$sum['spending'] ?? ''}}</span> (All Time)</div>
                            <div>---------------</div>
                            <div>Saldo saat ini: <span class="text-success"><strong>{{$sum['total'] ?? ''}}</strong></span></div>
                        </div>
                  
                        <div class="col">
                            <div class="text-right mb-4">
                                <a href="{{ route('transaction.index') }}" class="btn btn-primary ">Lihat Transaksi</a></div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
