@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">{{ __('Transaksi') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif



                    <form class="form" method="POST" action="{{ route('transaction.update', $data->id ) }}">
                        <input type="hidden" name="id" value="{{$data->id}}">
                        @csrf
                        <div class="form-group mt-3">
                            <label for="category_type">Tipe Kategori</label>
                            <select class="form-control @error('category_type') is-invalid @enderror" name="category_type" id="category_type" onchange="myFunction(value)">
                                <option>Pilih Tipe Kategori</option>
                                <option value="income">Pemasukan</option>
                                <option value="spending">Pengeluaran</option>
                            </select>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            @error('category_type')
                                    <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mt-3">
                            <label for="transaction_category">Kategori</label>
                            <select class="form-control @error('transaction_category') is-invalid @enderror" name="transaction_category" id="transaction_category">
                                <option>Pilih Kategori</option>
                                {{-- @foreach ($categories as $category)
                                    <option value="{{$category->id}}" {{$data->transaction_category == $category->id ? 'selected' : ''}}>{{$category->category_name}}</option>
                                @endforeach --}}
                            </select>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            @error('transaction_category')
                                    <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="transaction_amount">Nominal</label>
                        <input type="number" class="form-control @error('transaction_amount') is-invalid @enderror" id="transaction_amount" name="transaction_amount" value="{{$data->transaction_amount ?? ''}}"
                                 required>
                            <div class="valid-feedback">
                                Looks good!
                                
                            </div>
                            @error('transaction_amount')
                                    <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        {{-- <div class="form-group mb-3">
                            <label for="transaction_name">Nama Transaksi</label>
                            <input type="text" class="form-control @error('transaction_name') is-invalid @enderror" id="transaction_name" name="transaction_name"
                                 required>
                            <div class="valid-feedback">
                                Looks good!
                                
                            </div>
                            @error('transaction_name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div> --}}
                        <div class="form-group mb-3">
                            <label for="transaction_description">Catatan Transaksi</label>
                            <textarea class="form-control @error('transaction_description') is-invalid @enderror" id="transaction_description" name="transaction_description" rows="5">{{$data->transaction_description ?? ''}}</textarea>
                            <div class="valid-feedback">
                                Looks good!
                                
                            </div>
                            @error('transaction_description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <a href="{{ route('transaction.index') }}" class="btn btn-secondary">Kembali</a>

                        <button class="btn btn-primary" type="submit">Tambah</button>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection