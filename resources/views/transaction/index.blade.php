@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">{{ __('Transaksi') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col">
                            <div>Pemasukan <span class="text-success">{{$sum['income']}}</span></div>
                            <div>Pengeluaran <span class="text-danger">{{$sum['spending']}}</span></div>
                            <div>---------------</div>
                            <div>Total <span class="text-danger">{{$sum['total']}}</span></div>
                        </div>
                        <div class="col">
                            <div class="form-group mb-3">
                                <form action="{{ route('transaction.index') }}" method="GET">
                                    {{-- <input type="hidden" name="monthRangeStart">
                                    <input type="hidden" name="monthRangeEnd"> --}}
                                    <input type="date" class="form-control filter_date" name="yearRangeStart">                         
                                    <input type="date" class="form-control filter_date" name="yearRangeEnd">                         
                                    <button class="btn btn-primary" type="submit">Filter</button>
                                </form>
                            
                            </div>
                        </div>
                        <div class="col">
                            <div class="text-right mb-4">
                                <a href="{{ route('transaction.create') }}" class="btn btn-primary ">Tambah Transaksi</a></div>
                            </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nominal</th>
                                {{-- <th scope="col">Nama</th> --}}
                                <th scope="col">Catatan</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datas as $data)
                            <tr> 
                                @php
                                    switch ($data->category_type) {
                                        case 'income':
                                            $class = 'text-success';
                                            break;
                                        case 'spending':
                                            $class = 'text-danger';
                                            break;
                                    }
                                @endphp
                                <td><div class="{{$class}}">{{ $data->transaction_amount }}</div></td>
                                {{-- <td >{{ $data->transaction_name }}</td> --}}
                                <td>{{ $data->transaction_description }}</td>
                                <td>{{ $data->category_name }}</td>
                                <td>{{ $data->created_at }}</td>
                                <td>
                                    <div class="text-right">
                                        <a href="{{ route('transaction.edit', $data->id) }}" class="btn btn-primary">Edit</a>
                                        <a href="{{ route('transaction.destroy', $data->id) }}" class="btn btn-danger" onclick="return confirm('yakin?');">Hapus</a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        {{-- <tfoot>
                            <tr>
                                <td>2000</td>
                                <td>2000</td>
                                <td>2000</td>
                                <td>2000</td>
                                <td></td>
                            </tr>
                        </tfoot> --}}
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')

@endpush