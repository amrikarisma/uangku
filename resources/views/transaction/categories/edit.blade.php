@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">{{ __('Kategori') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form" method="POST" action="{{ route('category.update', $data['id'] ) }}">
                        @csrf
                        <div class="form-group mt-3">
                            <label for="category_type">Tipe Kategori</label>
                            <select class="form-control @error('category_type') is-invalid @enderror" name="category_type" id="category_type" required>
                                <option>Pilih Tipe Kategori</option>
                                <option value="income" {{ $data['category_type'] == 'income' ? 'selected' : '' }}>Pemasukan</option>
                                <option value="spending" {{ $data['category_type'] == 'spending' ? 'selected' : '' }}>Pengeluaran</option>
                            </select>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            @error('category_type')
                                    <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="category_name">Kategori</label>
                            <input type="text" class="form-control @error('category_name') is-invalid @enderror" id="category_name" name="category_name" value="{{$data['category_name']}}" placeholder="Contoh: Beli Pulsa"
                                 required>
                            <div class="valid-feedback">
                                Looks good!
                                
                            </div>
                            @error('category_name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="category_description">Deskripsi</label>
                            <input type="text" class="form-control @error('category_description') is-invalid @enderror" id="category_description" name="category_description" value="{{$data['category_description']}}"
                                 >
                            <div class="valid-feedback">
                                Looks good!
                                
                            </div>
                            @error('category_description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <a href="{{ route('category.index') }}" class="btn btn-secondary" type="submit">Kembali</a>
                        <button class="btn btn-primary" type="submit">Simpan</button>

                    </form>
                 
               


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
