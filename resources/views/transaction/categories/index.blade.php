@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">{{ __('Kategori') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col"></div>
                        <div class="col">
                            <div class="text-right mb-4">
                                <a href="{{ route('category.create') }}" class="btn btn-primary ">Tambah Kategori</a></div>

                            </div>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nama</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">Tipe</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datas as $data)
                            <tr>
                                <td >{{ $data['category_name'] }}</td>
                                <td>{{ $data['category_description'] }}</td>
                                <td>{{ $data['category_type'] }}</td>
                                <td>
                                    <div class="text-right">
                                        <a href="{{ route('category.edit', $data['id']) }}" class="btn btn-primary">Edit</a>
                                        <a href="{{ route('category.destroy', $data['id']) }}" class="btn btn-danger" onclick="return confirm('yakin?');">Hapus</a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                 
               


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
