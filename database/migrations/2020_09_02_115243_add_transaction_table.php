<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('transaction')) {
            //
        } else {
            Schema::create('transaction', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('transaction_category');
                // $table->unsignedBigInteger('transaction_user_id');
                // $table->string('transaction_name');
                $table->string('transaction_description')->nullable();
                $table->decimal('transaction_amount', 15, 0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
