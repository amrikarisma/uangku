<?php

namespace App\Http\Controllers;

use App\Model\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        
        $transaction = DB::table('transaction')
        ->whereBetween('transaction.created_at', [
            \Carbon\Carbon::createFromDate($request->yearRangeStart, $request->monthRangeStart)->startOfMonth(),
            \Carbon\Carbon::createFromDate($request->yearRangeEnd, $request->monthRangeEnd)->endOfMonth()
        ])
        ->leftJoin('categories', 'transaction.transaction_category', '=', 'categories.id')
        ->select('transaction.*','categories.category_name','categories.category_description','categories.category_type')
        ->orderby('transaction.created_at','ASC')
        ->get();
        
        $income = DB::table('transaction')
        ->where('categories.category_type', 'income')
        ->whereBetween('transaction.created_at', [
            \Carbon\Carbon::createFromDate($request->yearRangeStart, $request->monthRangeStart)->startOfMonth(),
            \Carbon\Carbon::createFromDate($request->yearRangeEnd, $request->monthRangeEnd)->endOfMonth()
        ])
        ->leftJoin('categories', 'transaction.transaction_category', '=', 'categories.id')
        ->sum('transaction_amount');

        $spending = DB::table('transaction')
        ->where('categories.category_type', 'spending')
        ->whereBetween('transaction.created_at', [
            \Carbon\Carbon::createFromDate($request->yearRangeStart, $request->monthRangeStart)->startOfMonth(),
            \Carbon\Carbon::createFromDate($request->yearRangeEnd, $request->monthRangeEnd)->endOfMonth()
        ])
        ->leftJoin('categories', 'transaction.transaction_category', '=', 'categories.id')
        ->sum('transaction_amount');



        return response([
            'success' => true,
            'message' => 'List Semua Kategori',
            'data' => [
                'list' => $transaction,
                'income' => $income,
                'spending' => $spending,
                'total' => ($income+$spending)
            ]
        ], 200);
        // return view('transaction/index', [
        //     'datas' => $transaction,
        //     'sum' => [
        //         'income' => $income,
        //         'spending' => $spending,
        //         'total' => $total = $income+$spending
        //     ]
        //     ]);
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = DB::table('categories')
        ->get();
        // return $categories[0]->id;
        return view('transaction/add', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->transaction_category) && $request->transaction_category != "Pilih Kategori"){

            $type = DB::table('categories')
            ->where('id', $request->transaction_category)
            ->get('category_type')
            ->first();

            if($type != null){
                $this->validate($request, [
                    'transaction_category'	    => 'required|integer',
                    // 'transaction_name'	        => 'required|unique:transaction|string',
                    // 'transaction_description'	=> 'string',
                    'transaction_amount'	=> 'required|integer'
                ]);
                
                $transaction = Transaction::create([
                    'transaction_category' => $request->transaction_category,
                    // 'transaction_name' => $request->transaction_name,
                    'transaction_description' => $request->transaction_description,
                    'transaction_amount' => $type->category_type == 'income' ? (int)($request->transaction_amount * 1) : (int)($request->transaction_amount * -1)
                    ]);
        
                if ($transaction) {
                    return response()->json([
                        'success' => true,
                        'message' => 'Transaksi Berhasil Disimpan!',
                    ], 200);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Transaksi Gagal Disimpan!',
                    ], 400);
                }
            }
            return response()->json([
                'success' => false,
                'message' => 'Tipe kategori tidak ditemukan!',
            ], 400);
        }
        return response()->json([
            'success' => false,
            'message' => 'Isi field dengan benar!',
        ], 400);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datas = Transaction::where('transaction.id', $id)
        ->select('transaction.*','categories.category_name','categories.category_description','categories.category_type')
        ->leftJoin('categories', 'transaction.transaction_category', '=', 'categories.id')
        ->get()->first();
        return response([
            'success' => true,
            'message' => 'Detail Transaksi',
            'data' => $datas
        ], 200);    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        $categories = DB::table('categories')
        ->get();
        $data = DB::table('transaction')
        ->where('id', $transaction->id)
        ->get()
        ->first();

        // return $data->id;
        return view('transaction/edit', [
            'data' => $data,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        if(isset($request->transaction_category) && $request->transaction_category != "Pilih Kategori"){

            $this->validate($request, [
                'transaction_category'	    => 'required',
                // 'transaction_name'	        => 'required|unique:transaction|string',
                // 'transaction_description'	=> 'string',
                'transaction_amount'	    => 'required|integer'
            ]);

            // return $request->id;
            $type = DB::table('categories')
            ->where('id', $request->transaction_category)
            ->get('category_type')
            ->first();

            if($type != null){
                if($type->category_type == 'income' && $request->transaction_amount > 0 ) {
                    $amount = (int)($request->transaction_amount);
                } elseif($type->category_type == 'income' && $request->transaction_amount < 0 ) {
                    $amount = (int)($request->transaction_amount * -1);
                } elseif($type->category_type == 'spending' && $request->transaction_amount > 0 ) {
                    $amount = (int)($request->transaction_amount * -1);
                } else {
                    $amount = (int)($request->transaction_amount * 1);
                }

                $transaction = Transaction::find($request->id)->update([
                    'transaction_category' => $request->transaction_category,
                    // 'transaction_name' => $request->transaction_name,
                    'transaction_description' => $request->transaction_description,
                    'transaction_amount' => $amount
                ]);

                if ($transaction) {
                    return response()->json([
                        'success' => true,
                        'message' => 'Transaksi Berhasil Disimpan!',
                    ], 200);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Transaksi Gagal Disimpan!',
                    ], 400);
                }
  
            }
            return response()->json([
                'success' => false,
                'message' => 'Tipe kategori tidak ditemukan!',
            ], 400);
        }
        return response()->json([
            'success' => false,
            'message' => 'Isi field dengan benar!',
        ], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Transaction::where('id',$id);
        $data->delete();
        return response([
            'success' => true,
            'message' => 'Data berhasil dihapusi!'
        ], 200);
        // return redirect(route('transaction.index'))->with('status', 'Data berhasil dihapus!');
    }
}
