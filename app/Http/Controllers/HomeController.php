<?php

namespace App\Http\Controllers;

use App\Model\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $income = DB::table('transaction')
        ->where('categories.category_type', 'income')
        ->leftJoin('categories', 'transaction.transaction_category', '=', 'categories.id')
        ->sum('transaction_amount');
        $spending = DB::table('transaction')
        ->where('categories.category_type', 'spending')
        ->leftJoin('categories', 'transaction.transaction_category', '=', 'categories.id')
        ->sum('transaction_amount');

        // dd($datas);
        return view('home', [
            // 'datas' => $transaction,
            'sum' => [
                'income' => $income,
                'spending' => $spending,
                'total' => $total = $income+$spending
            ]
            ]);
    }
}
