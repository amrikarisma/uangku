<?php

namespace App\Http\Controllers\API;

use App\Model\Categories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_type($id)
    {
        $datas = Categories::where('category_type', $id)
        ->get();
        return [
            'datas' => $datas,
        ];

    }
}
