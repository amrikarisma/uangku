<?php

namespace App\Http\Controllers;

use App\Model\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->category_type)):
            $datas = Categories::where('category_type', $request->category_type)->get();
        else : 
            $datas = Categories::get();
        endif;

        return response([
            'success' => true,
            'message' => 'List Semua Kategori',
            'data' => $datas
        ], 200);
        // return view('transaction/categories/index', [
        //     'datas' => $datas,
        // ]);
  

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaction/categories/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validate($request, [
            'category_type'	        => 'required|in:income,spending',
            'category_name'	        => 'required|unique:categories|string',
            // 'category_description'	=> 'string'
        ],
        [
            'category_type.required' => 'Masukkan Tipe Kategori!',
            'category_name.required' => 'Masukkan Nama Kategori!',
        ]);
        
        $category = Categories::create([
            'category_type' => $request->category_type,
            'category_name' => $request->category_name,
            'category_description' => $request->category_description
        ]);

        if ($category) {
            return response()->json([
                'success' => true,
                'message' => 'Kategori Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Kategori Gagal Disimpan!',
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $datas = Categories::where('id', $id)->get()->first();
        return response([
            'success' => true,
            'message' => 'Detail Kategori',
            'data' => $datas
        ], 200);    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Categories $categories)
    {
        $data = Categories::find($id);

        return view('transaction/categories/edit', [
            'data' => $data,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categories $categories)
    {
        $this->validate($request, [
            'category_type'	        => 'required|in:income,spending',
            'category_name'	        => 'required|string',
            // 'category_description'	=> 'string'
        ]);
        $data = Categories::find($request->id)->update([
                'category_type' => $request->category_type,
                'category_name' => $request->category_name,
                'category_description' => $request->category_description
        ]);
        return response([
            'success' => true,
            'message' => 'Berhasil update Kategori!'
        ], 200);
        // return redirect(route('category.index'))->with('status', 'Berhasil update Kategori!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Categories $categories)
    {
        $data = Categories::where('id',$id);
        $data->delete();
        return response([
            'success' => true,
            'message' => 'Data berhasil dihapusi!'
        ], 200);
        // return redirect(route('category.index'))->with('status', 'Data berhasil dihapus!');

    }
}
