<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'transaction';
   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
       'transaction_name', 'transaction_description', 'transaction_category', 'transaction_amount'
   ];
}
